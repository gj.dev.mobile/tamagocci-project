﻿/// <reference path="../spec/hellokitty-spec.js" />
/// <reference path="tamagocci.js" />

// Definition of HelloKitty
define(['exports', 'app/Tamagocci'], function (exports, Tamagocci) {

    HelloKitty = function () {
        this.goodPicture = "hk_good.gif"
       , this.badPicture = "hk_bad.gif"
       , this.deadPicture = "hk_dead.png";
    };

    HelloKitty.prototype = new Tamagocci.Tamagocci();

    exports.HelloKitty = HelloKitty;

});