﻿/// <reference path="../spec/hellokitty-spec.js" />
/// <reference path="tamagocci.js" />

// Definition of Pikachu
define(['exports', 'app/Tamagocci'], function (exports, Tamagocci) {

    Pikachu = function () {
        this.goodPicture = "pk_good.gif"
      , this.badPicture = "pk_bad.gif"
      , this.deadPicture = "pk_dead.gif"
    };

    Pikachu.prototype = new Tamagocci.Tamagocci();

    exports.Pikachu = Pikachu;

});