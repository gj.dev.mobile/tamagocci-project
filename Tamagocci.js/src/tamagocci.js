/// <reference path="../spec/hellokitty-spec.js" />
/// <reference path="tamagocci.js" />

/*
import watchJS to see when attributes value change.
source : https://github.com/melanke/Watch.JS
*/

// Definition of Tamagocci
define(['exports', 'watch'], function (exports, watchJS) {

    Tamagocci = function () {

        var _thisTamagocci = this;
        this.weight = 5
        , this.happiness = 5
        , this.age = 0
        , this.minWeight = 1
        , this.maxWeight = 10
        , this.isDead = false
        , this.goodPicture = null
        , this.badPicture = null
        , this.deadPicture = null

        //define available events
        // default : die
        , this.events = { 'die': function () { console.log("You are Dead Y_Y") } };


        this.onWeightChange = function () {
            this.isDead = this.isDeadTest();
        }
        this.onHappinessChange = function () {
            this.isDead = this.isDeadTest();
        }

        //  entries :
        //      event : the event to listen
        //      callback : the callback to call when event is fired
        this.on = function (event, callback) {
            _thisTamagocci.events[event] = callback;
        }

        //  entries :
        //      event : the event to fire
        // if event not registred before with 'on', has no effect
        this.dispatchEvent = function (event) {
            try {
                _thisTamagocci.events[event]();
            } catch (e) {
                console.log(" Error : can't dispatch " + event + ", no function attached");
            }
        };

        this.eat = function () {
            this.weight += 2;
        }

        this.play = function () {
            this.weight -= 1;
            this.happiness += 1;
        }

        this.becomeOlder = function () {
            _thisTamagocci.age++;
            _thisTamagocci.minWeight++;
            _thisTamagocci.maxWeight++;
            _thisTamagocci.happiness--;
        }

        this.getPicture = function () {
            if (this.isDead)
                return this.deadPicture;
            if (this.isBadHealth())
                return this.badPicture;
            return this.goodPicture;
        }

        this.isDeadTest = function () {
            return this.weight < this.minWeight || this.weight > this.maxWeight || this.happiness <= 0;
        }

        this.isBadHealth = function () {
            return this.weight < this.minWeight + 3 || this.weight > this.maxWeight - 3 || this.happiness < 3;
        }

        //Call this to unbind the watcher
        this.destroy = function () {
            console.log("__destroy Object__");
            watchJS.unwatch(this, ["weight", "happiness", "isDead"],
                            this.onWeightChange, this.onHappinessChange, this.ondie);
        }

        this.OnDieChange = function () {
            this.dispatchEvent("die");
        }

        //Fire Event when attributes change
        watchJS.watch(this, ["weight"], this.onWeightChange);
        watchJS.watch(this, ["happiness"], this.onHappinessChange);
        watchJS.watch(this, ["isDead"], this.OnDieChange);
    };

    exports.Tamagocci = Tamagocci;

});